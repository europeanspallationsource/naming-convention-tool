# Overview
This project includes all Naming Convention Tool sources.

All projects are Eclipse projects and are configured to use and to be build with maven. Projects are interdependent.

## Projects
- NamingConventionTool
    Main web-base application to manage naming convention of a particle accelerator facility.  

- NamingConventionTool-JAXB  
	Provides JAXB implementation of POJOs and Resource definitions used by NamingConventionTool webservice NamingConventionTool-Client.

- NamingConventionTool-Client  
	The client api to access the NamingConventionTool webservice. It provides the ability to retrieve names data registered in NamingConventionTool.

# Build Projects

All projects are configured to be build by maven. The projects extend the ess-java-config pom.xml, which has to be available in the maven repository, before RBAC can be build. You can get it here:  
[https://bitbucket.org/ess_ics/ess-java-config](https://bitbucket.org/europeanspallationsource/ess-java-config)  
First, install the ess-java-config by running command **mvn install**. This will install the ess-java-config pom.xml into your maven repository. Now you can build the Naming Convention Tool code. You can do that by executing **mvn install** in each individual project, or execute **mvn install** in the Naming Convention Tool root. The root pom.xml contains references to all Naming Convention Tool projects and will build and install them in proper order. The output of the build is located in the *target* folder of each individual project.

# Docker

The application can be built and run as a Docker container. The image is based on the jboss/wildfly image from Docker Hub.

### How to use this image

```
$ docker run registry.esss.lu.se/ics-software/naming-convention-tool
```

Environment variables that can be set when running a container based on this image:

| Environment variable     | Default    | Description |
| -------------------------|------------|-------------|
| NAMING_DEPLOYMENT_CONTEXT_ROOT | / | Context root used for the application |
| NAMING_SWAGGER_BASEPATH | /rest | Basepath used in OAS |
| NAMING_SWAGGER_SCHEMES | https | Schemes used in OAS |
| CCDB_URL | https://ccdb.esss.lu.se/ | URL for CCDB |
| RBAC_PRIMARY_URL | https://rbac.esss.lu.se:8443/service | URL for primary RBAC service |
| RBAC_PRIMARY_SSL_HOST | rbac.esss.lu.se | SSL host for primary RBAC service |
| RBAC_PRIMARY_SSL_PORT | 8443 | SSL port for primary RBAC service |
| RBAC_SECONDARY_URL | https://localhost:8443/service | URL for secondary RBAC service |
| RBAC_SECONDARY_SSL_HOST | localhost | SSL host for secondary RBAC service |
| RBAC_SECONDARY_SSL_PORT | 8443 | SSL port for secondary RBAC service |
| RBAC_SINGLE_SIGNON | false | Use single sign-on |
| RBAC_HANDSHAKE | true | Perform SSL handshake with RBAC |
| RBAC_HANDSHAKE_TIMEOUT | 2000 | Timeout for SSL handshake with RBAC |
| RBAC_INACTIVITY_TIMEOUT_DEFAULT | 900 | Inactivity timeout for RBAC client |
| RBAC_INACTIVITY_RESPONSE_GRACE | 30 | Inactivity response grace period for RBAC client |
| RBAC_SHOW_ROLE_SELECTOR | false | Show role selector in RBAC client |
| RBAC_VERIFY_SIGNATURE | false | Verify signature of RBAC |
| RBAC_PUBLIC_KEY_LOCATION | ~/.rbac/rbac.key | Public key for RBAC |
| RBAC_LOCAL_SERVICES_PORT | 9421 | Port for local RBAC service |
| RBAC_USE_LOCAL_SERVICE | false | Use local RBAC service |
| RBAC_CERTIFICATE_STORE | ~/.rbac | SSL certificate store for RBAC client |

### Docker Compose

For convenience, the application comes with a docker-compose.yml file, which can be used to run the application with required services and configuration:

```
$ docker-compose up
```
