package org.openepics.names.jaxb;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

/**
 * This resource provides bulk device name data, and has a subresource for
 * retrieving data of specific device names.
 *
 * @author Andraz Pozar
 * @author Sunil Sah
 * @author Banafsheh Hajinasab
 *
 */
@Path("deviceNames")
@Api(value = "/deviceNames")
//@Api(tags = {"naming"})
@Produces({"application/json"})
@SwaggerDefinition(
        info = @Info(
                description = "\n"
                + "    This is a documentation for all REST interfaces of Naming Service.  You can find \n"
                + "    out more about Naming Service at \n"
                + "    [https://confluence.esss.lu.se/display/NC]",
                version = "1.0.2",
                title = "Naming service API documentation",
                termsOfService = "http://swagger.io/terms/",
                contact = @Contact(
                        name = "Anders Harrisson, Banafsheh Hajinasab",
                        email = "anders.harrisson@esss.se",
                        url = "https://confluence.esss.lu.se/display/NC")))

public interface DeviceNamesResource {

    @GET
    @ApiOperation(value = "Finds all devices",
            notes = "Returns a list of names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNames();

    @Path("{uuid}")
    public SpecificDeviceNameResource getSpecificDeviceNameSubresource();

    @GET
    @Path("subsection/{subsection}")
    @ApiOperation(value = "Finds all devices by subsection",
            notes = "Returns a list of names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesBySubSection(@PathParam("subsection") String subSection);

    @GET
    @Path("section/{section}")
    @ApiOperation(value = "Finds all devices by section",
            notes = "Returns a list of names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesBySection(@PathParam("section") String section);

    @GET
    @Path("discipline/{discipline}")
    @ApiOperation(value = "Finds all devices by discipline",
            notes = "Returns a list of names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesByDiscipline(@PathParam("discipline") String discipline);

    @GET
    @Path("devicetype/{devicetype}")
    @ApiOperation(value = "Finds all devices by device type",
            notes = "Returns a list of names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesByDeviceType(@PathParam("devicetype") String deviceType);

    @GET
    @Path("search/{searchText}")
    @ApiOperation(value = "Finds all devices containing the a specific search text",
            notes = "Returns a list of names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesByTextSearch(@PathParam("searchText") String searchText);

}
