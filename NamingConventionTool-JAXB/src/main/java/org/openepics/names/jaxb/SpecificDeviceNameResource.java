package org.openepics.names.jaxb;

import io.swagger.annotations.ApiOperation;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides specific device name data.
 *
 * @author Andraz Pozar
 * @author Sunil Sah
 * @author Banafsheh Hajinasab

 */
public interface SpecificDeviceNameResource {

    /**
     *
     * @param string uuid or name
     * @return The most recent deviceNameElemt with the specified name or uuid
     */
    
    @GET
    @ApiOperation(value = "Finds device by UUID or Name",
    notes = "Returns a single name",
    response = DeviceNameElement.class)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public DeviceNameElement getDeviceName(@PathParam("uuid") String string);

    public DeviceNameElement getDeviceNameBySubSection(String subSection);

    public DeviceNameElement getDeviceNameBySection(String section);

    public DeviceNameElement getDeviceNameByDiscipline(String discipline);

    public DeviceNameElement getDeviceNameByDeviceType(String deviceType);
    
    public DeviceNameElement getDeviceNameByTextSearch(String text);

}
