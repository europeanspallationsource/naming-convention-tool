#Settings for database stress testing
#
#####################################
#Amount of population data settings
# Minimum amount of any imported entity is 1.
devices = 5000

#Database connection settings
databaseUser = 'discs_names'
databasePassword = 'discs_names'
databaseHost = 'localhost'
databasePort = 5432
databaseName = 'discs_names'