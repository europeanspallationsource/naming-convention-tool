#!/usr/bin/python
#
#Version: 0.01a
#
#Requirements to run this script : python and pg8000
#To install pg8000 : pip install pg8000
#

import pg8000 as pg
from config import *

print('Connecting do database')

#Connection settings are in config.py
connection = pg.connect(user = databaseUser, password = databasePassword, host = databaseHost, port = databasePort, database = databaseName)
cursor = connection.cursor()

print('DELETING...')
print('DELETING DEVICES')
cursor.execute("TRUNCATE device CASCADE")
print('DELETING DEVICEREVISONS')
cursor.execute("TRUNCATE devicerevision CASCADE")
print('DELETING NAMEPARTREVISIONS')
cursor.execute("TRUNCATE namepartrevision CASCADE")
print('DELETING NAMEPARTS')
cursor.execute("TRUNCATE namepart CASCADE")

connection.commit()
print('Closing connection...')

cursor.close()
connection.close()

print('Done')



################################################
#This is code for cleaning only data created by populate script. it is not maintained, and is here only in case of a redesign of a cleanup script.
# import pg8000 as pg
# from classes import *
# from config import *
# ################
# ####SETTINGS####
# ################
# ##Needs to be set at same values as in creation script
# 
# ownerID = 9
# mnemonics = ["Sec", "Sub", "Dis", "Dev", "Mev","Mis"]
# ##################
# #clears data from database
# 
# 
# #######################
# ####DEVICEREVISIONS####
# #######################
# devicerevisionNames = []
# for i in range(devicerevisions):
#    devicerevisionNames.append(DeviceRevision(i , i//nameparts, 0 , 0 , 0, ownerID).conventionname)
# #########################
# ####NAMEPARTREVISIONS####
# #########################
# #Generating Data
# namepartrevisionNames = []
# #PARENTS
# namepartrevisionNames.append(NamePartRevision(0, 0, DeviceRevision.deviceTypeParents[0], 0, ownerID).name)
# namepartrevisionNames.append(NamePartRevision(0, 0, DeviceRevision.sectionParents[0],  0, ownerID).name)
# namepartrevisionNames.append(NamePartRevision(0, 0, DeviceRevision.deviceTypeParents[1], 0, ownerID).name)
# namepartrevisionNames.append(NamePartRevision(0, 0, DeviceRevision.deviceTypeParents[2], 0, ownerID).name)
# namepartrevisionNames.append(NamePartRevision(0, 0, DeviceRevision.sectionParents[1], 0, ownerID).name)
# namepartrevisionNames.append(NamePartRevision(0, 0, DeviceRevision.sectionParents[2], 0, ownerID).name)
# for i in range(namepartrevisions/2 - 1):
#     namepartrevisionNames.append(NamePartRevision(i, i//(namepartrevisions/2 - 1), mnemonics[i% len(mnemonics)], 0, ownerID).name)
# for i in range(namepartrevisions/2 - 1):
#     namepartrevisionNames.append(NamePartRevision(i + namepartrevisions/2, i//(namepartrevisions/2 - 1), mnemonics[i% len(mnemonics)], 0, ownerID, 0).name)
#     
# ###############
# ####DATABSE####
# ###############
# print('Connecting do database')
# connection = pg.connect(user = databaseUser, password = databasePassword, host = databasehost, port = databasePort, database = databaseName)
# cursor = connection.cursor()
# 
# 
# 
# print('Getting devicerevision IDs')
# cursor.execute("SELECT id FROM devicerevision WHERE requestedby_id = '" + str(ownerID) + "' AND conventionname IN ('" + "', '".join(devicerevisionNames) + "')")
# devicerevisionIDs = [str(id[0]) for id in cursor.fetchall()]
# 
# print('Getting device IDs')
# cursor.execute("SELECT device_id FROM devicerevision WHERE id IN ('" + "', '".join(devicerevisionIDs) + "')")
# deviceIDs = [str(id[0]) for id in cursor.fetchall()]
# 
# 
# print('Getting namepartrevisions IDs')
# cursor.execute("SELECT id FROM namepartrevision WHERE processedby_id = " + str(ownerID) + "AND requestedby_id = " + str(ownerID) +  "AND name IN ('" + "', '".join(namepartrevisionNames) + "')")
# namepartrevisionIDs = [str(id[0]) for id in cursor.fetchall()]
# 
# print('Getting namepart IDs')
# cursor.execute("SELECT namepart_id FROM namepartrevision WHERE id IN ('" + "', '".join(namepartrevisionIDs) + "')")
# namepartIDs = [str(id[0]) for id in cursor.fetchall()]
# 
# 
# 
# 
# print('DELETING...')
# cursor.execute("DELETE FROM devicerevision WHERE id IN ('" + "', '".join(devicerevisionIDs) + "')")
# cursor.execute("DELETE FROM device WHERE id IN ('" + "', '".join(deviceIDs) + "')")
# 
# cursor.execute("DELETE FROM namepartrevision WHERE id IN ('" + "', '".join(namepartrevisionIDs) + "')")
# cursor.execute("DELETE FROM namepart WHERE id IN ('" + "', '".join(namepartIDs) + "')")
# 
# connection.commit()
# print('Closing connection...')
# 
# cursor.close()
# connection.close()
# 
# print('Done')
