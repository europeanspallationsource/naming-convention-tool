/*-
* Copyright (c) 2014 European Spallation Source
* Copyright (c) 2014 Cosylab d.d.
*
* This file is part of Naming Service.
* Naming Service is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or any newer version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
*/
package org.openepics.names.webservice;

import java.util.Set;

import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.config.*;
/**
 * This represents the JAX-RS application which hosts all REST resources of the naming tool.
 *
 * @author Andraz Pozar 
 */
@javax.ws.rs.ApplicationPath("/rest")
public class NameRestService extends Application {

    public NameRestService() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(System.getProperty("names.swagger.schemes", "https").split(","));
        beanConfig.setBasePath(System.getProperty("names.swagger.basepath", "/rest"));
        beanConfig.setResourcePackage("org.openepics.names.jaxb");
        beanConfig.setScan(true);
        
    }

    @Override
    public Set<Class<?>> getClasses() {
        return getRestResourceClasses();
    }

    private Set<Class<?>> getRestResourceClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        return resources;
    }
}
