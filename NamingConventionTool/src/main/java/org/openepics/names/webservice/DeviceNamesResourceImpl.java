/*-
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.webservice;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.jaxb.SpecificDeviceNameResource;
import org.openepics.names.nameViews.NameViewProvider;
import org.openepics.names.jaxb.DeviceNamesResource;
import com.google.common.collect.Lists;
import java.util.regex.Pattern;

/**
 * This is implementation of {@link DeviceNamesResource} interface.
 *
 * @author Andraz Pozar
 * @author Banafsheh Hajinasab
 *
 */
@Stateless
public class DeviceNamesResourceImpl implements DeviceNamesResource {

    @Inject
    private SpecificDeviceNameResource specificDeviceNameSubresource;
    @Inject
    private NameViewProvider nameViewProvider;

    @Override
    public List<DeviceNameElement> getAllDeviceNames() {
        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (String name : nameViewProvider.getNameRevisions().keySet()) {
            DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceName(name);
            deviceNames.add(deviceData);
        }
        return deviceNames;
    }

    @Override
    public SpecificDeviceNameResource getSpecificDeviceNameSubresource() {
        return specificDeviceNameSubresource;
    }

    @Override
    public List<DeviceNameElement> getAllDeviceNamesBySubSection(String subSection) {
        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (String name : nameViewProvider.getNameRevisions().keySet()) {

            String simpleName = name.toLowerCase();
            String sub = simpleName.substring(simpleName.indexOf("-") + 1, simpleName.indexOf(":"));

            if (sub.equalsIgnoreCase(subSection)) {

                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameBySubSection(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }

    @Override
    public List<DeviceNameElement> getAllDeviceNamesBySection(String section) {
        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (String name : nameViewProvider.getNameRevisions().keySet()) {

            String simpleName = name.toLowerCase();
            String sub = simpleName.substring(0, simpleName.indexOf("-"));

            if (sub.equalsIgnoreCase(section)) {

                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameBySection(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }

    @Override
    public List<DeviceNameElement> getAllDeviceNamesByDiscipline(String discipline) {
        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (String name : nameViewProvider.getNameRevisions().keySet()) {

            String simpleName = name.toLowerCase();
            String sub = simpleName.substring(simpleName.indexOf(":") + 1);
            sub = sub.substring(0, sub.indexOf("-"));

            if (sub.equalsIgnoreCase(discipline)) {

                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameByDiscipline(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }

    @Override
    public List<DeviceNameElement> getAllDeviceNamesByDeviceType(String deviceType) {
        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (String name : nameViewProvider.getNameRevisions().keySet()) {

            String simpleName = name.toLowerCase();
            String sub = simpleName.substring(simpleName.indexOf("-") + 1);
            sub = sub.substring(sub.indexOf("-") + 1);

            if (sub.equalsIgnoreCase(deviceType)) {

                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameByDeviceType(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }

    @Override
    public List<DeviceNameElement> getAllDeviceNamesByTextSearch(String searchText) {
        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (String name : nameViewProvider.getNameRevisions().keySet()) {
            
            String str = name.toLowerCase();

            if (Pattern.compile(Pattern.quote(searchText), Pattern.CASE_INSENSITIVE).matcher(str).find()) {
                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameByTextSearch(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }
    
}
