package org.openepics.names.operation;

import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.NameType;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.util.As;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.openepics.names.util.ValidationException;

/**
 * Class to handle information of the operation to be executed.
 *
 * @author karinrathsman
 *
 */
public abstract class NameOperation {

    private List<NameView> affectedNameViews;
    private NameView root;
    private Set<NameView> selectedNameViews;
    private String message;
    private Map<NameView, NameRevision> originalRevisionMap;
    private List<NameRevision> newRevisions;

    /**
     * Constructor updates the selectedNamesView
     *
     * @param selectedNameViews the selected name views that the operation will be executed on
     * @param root the root of the hierarchy (Area structure or device structure) .
     */
    public NameOperation(Set<NameView> selectedNameViews, NameView root) {
        Preconditions.checkState(root != null && root.isRoot());
        this.root = root;
        this.selectedNameViews = selectedNameViews;
        this.newRevisions = Lists.newArrayList();
    }

    /**
     * updates after e.g. a new selection.
     */
    protected void update() {
        this.affectedNameViews = affectedNameViews();
        this.originalRevisionMap = originalRevisionMap();
    }

    /**
     *
     * @return list of devices affected by this operation
     */
    public abstract Set<NameView> getAffectedDevices();

    /**
     *
     * @param stage before executing the operation
     * @return stage after executing the operation. null if execution is not allowed
     */
    @Nullable
    public abstract NameStage operatedStage(NameStage stage);

    /**
     * @return the selectedNameViews
     */
    public Set<NameView> getSelectedNameViews() {
        return selectedNameViews;
    }

    /**
     *
     * @return the set of nameViews to be operated on.
     */
    public List<NameView> getAffectedNameViews() {
        return affectedNameViews;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = As.nullIfEmpty(message);
    }

    /**
     *
     * @return the title used in operation headers
     */
    public abstract String getTitle();

    /**
     * @return the result (deleted, approved, rejected...) as a string
     */
    public abstract String getResult();

    /**
     *
     * @return Message to be displayed after a successful operation has been executed
     */
    public String getSuccessMessage() {
        int n = affectedNameViews.size();
        return n + (n > 1 ? " names have been " : " name has been ") + getResult();
    }

    /**
     * Creates the originalNameRevisionMap, which is used to check if there are new revisions since the class was updated.
     *
     * @return the original Name revision map.
     */
    private Map<NameView, NameRevision> originalRevisionMap() {
        Map<NameView, NameRevision> map = Maps.newHashMap();
        for (NameView nameView : affectedNameViews) {
            if (!nameView.isRoot()) {
                map.put(nameView, nameView.getRevisionPair().getLatestRevision());
            }
        }
        return map;
    }

    /**
     *
     * @return nameViews from nameViews that has new revisions since the class was initiated.
     */
    public Set<NameView> nameViewsWithUpdatedRevisions() {
        Set<NameView> updatedNameViews = Sets.newHashSet();
        List<NameView> affectedNameViews = affectedNameViews();
        for (NameView nameView : affectedNameViews) {
            if (!nameView.isRoot() && (!originalRevisionMap.containsKey(nameView) || nameView.getRevisionPair().getLatestRevision().supersede(originalRevisionMap.get(nameView)))) {
                updatedNameViews.add(nameView);
            }
        }
        for (NameView nameView : originalRevisionMap.keySet()) {
            if (!affectedNameViews.contains(nameView)) {
                updatedNameViews.add(nameView);
            }
        }
        return updatedNameViews;
    }

    /**
     * Sets the nameViews that will be operated on based on the specified selection
     *
     * @return list with the name views that are affected by the operation
     */
    protected abstract List<NameView> affectedNameViews();

    /**
     *
     * @param parentView the parent of the nameView
     * @param nameView the nameView
     * @return true if the operation on nameView or parentView is allowed
     */
    protected boolean allowedAsParentChild(NameView parentView, NameView nameView) {
        NameStage nameStage = operatedOrUnaffectedStage(nameView);
        NameStage parentStage = operatedOrUnaffectedStage(parentView);
        return parentStage != null && nameStage != null && parentStage.isAllowedAsParentOf(nameStage);
    }

    /**
     *
     * @param nameView the nameView that the nameStage will be derived for
     * @return the nameStage after operation if the nameView is included, otherwise the unoperated name stage
     */
    public NameStage operatedOrUnaffectedStage(NameView nameView) {
        NameStage stage = nameStage(nameView);
        return getAffectedNameViews().contains(nameView) ? operatedStage(stage) : stage;
    }

    /**
     *
     * @param nameView the nameView to be tested
     * @return true if the operation affects the nameView
     */
    public abstract boolean affects(NameView nameView);

    /**
     * @return true if message is required, false if optional.
     */
    public abstract boolean isMessageRequired();

    /**
     * Validates that the operation applies to the selected set of Name views.
     *
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public abstract void validateOnSelect() throws ValidationException;

    /**
     *
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public abstract void validate() throws ValidationException;

    /**
     * Validates the operation data is up to date.
     *
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public void validateUpToDate() throws ValidationException {
        As.validateState(nameViewsWithUpdatedRevisions().isEmpty(), "The operation data is not up to date");
    }

    /**
     * @return the root
     */
    public NameView getRoot() {
        return root;
    }

    /**
     *
     * @return the root type
     */
    public NameType rootType() {
        return getRoot().getNameType();
    }

    /**
     *
     * @return the list of new revision resulting from the operation.
     */
    public List<NameRevision> getNewRevisions() {
        return newRevisions;
    }

    /**
     * Adds a new revision to the list of revisions.
     *
     * @param nameRevision the new nameRevision to be added after operation
     */
    public void addNewRevision(NameRevision nameRevision) {
        newRevisions.add(nameRevision);
    }

    /**
     *
     * @return the complement to root type
     */
    public NameType otherType() {
        return rootType().isAreaStructure() ? NameType.DEVICE_STRUCTURE : NameType.AREA_STRUCTURE;
    }

    /**
     *
     * @param nameView the name View
     * @return nameStage of the nameView. INITIAL if nameView is null.
     */
    protected static NameStage nameStage(NameView nameView) {
        return nameView != null ? nameView.getRevisionPair().getNameStage() : NameStage.INITIAL;
    }

    /**
     * Validates the user role
     *
     * @param editor true if user is editor
     * @param superUser true if the user is superUser
     * @throws org.openepics.names.util.ValidationException Name validation exception
     */
    public void validateUser(boolean editor, boolean superUser) throws ValidationException {
        As.validateState(editor, "User is not authorized");
    }

}
