package org.openepics.names.operation;

import java.util.Set;

import org.openepics.names.business.NameStage;
import org.openepics.names.nameViews.NameView;

public class ModifyDevice extends Modify {

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public ModifyDevice(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public boolean acceptedAsParent(NameView parentView) {
        return super.acceptedAsParent(parentView) && parentView.isChildInDeviceRegistry();
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        NameStage requestStage = super.operatedStage(stage);
        return requestStage != null ? requestStage.nextProcessedStage(true) : null;
    }

    @Override
    public String getResult() {
        return "modified";
    }

    @Override
    public String getTitle() {
        return "Modify ";
    }

    @Override
    public boolean affects(NameView nameView) {
        return super.affects(nameView) && nameView.isInDeviceRegistry();
    }

}
