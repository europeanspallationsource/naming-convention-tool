package org.openepics.names.operation;

import java.util.Set;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

public class DeleteDevices extends Delete {

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public DeleteDevices(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        NameStage deleteStage = super.operatedStage(stage);
        return deleteStage != null ? deleteStage.nextProcessedStage(true) : null;
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        super.validateOnSelect();
        for (NameView nameView : getAffectedNameViews()) {
            As.validateState(nameView.isInDeviceRegistry(), "Name is not a device name");
        }
    }

    @Override
    public String getResult() {
        return "deleted";
    }

    @Override
    public String getTitle() {
        return "Delete";
    }

    @Override
    protected boolean affectsDevices(NameView nameView) {
        return false;
    }

    @Override
    protected NameRevision nextApprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getApprovedRevision();
    }

    @Override
    protected NameRevision nextUnapprovedRevision(NameView nameView) {
        return null;
    }
}
