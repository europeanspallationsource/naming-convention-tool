/*-
* Copyright (c) 2014 European Spallation Source
* Copyright (c) 2014 Cosylab d.d.
*
* This file is part of Naming Service.
* Naming Service is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or any newer version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.util;

import javax.annotation.Nullable;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

/**
 * A static utility class for reading single Excel file cell
 *
 * @author Andraz Pozar
 */
public class ExcelCell {

    /**
     * Creating a String from Excel file cell.
     *
     * @param cell Excel file cell.
     * @return Cell value as string. If cell contains numeric value, this value is cast to String. If there is no value for this cell, null is returned.
     */
    public static @Nullable
    String asString(@Nullable Cell cell) {
        if (cell != null) {
            if (null == cell.getCellTypeEnum()) {
                throw new UnsupportedOperationException();
            } else switch (cell.getCellTypeEnum()) {
                case NUMERIC:
                    return String.valueOf(cell.getNumericCellValue());
                case STRING:
                    return cell.getStringCellValue() != null ? cell.getStringCellValue() : null;
                case BLANK:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        } else {
            return null;
        }
    }

    /**
     * Reading Excel file cell with numeric value and returning its value
     *
     * @param cell Excel file cell.
     * @return Cell value as number.
     */
    public static double asNumber(Cell cell) {
        return cell.getNumericCellValue();
    }
}
