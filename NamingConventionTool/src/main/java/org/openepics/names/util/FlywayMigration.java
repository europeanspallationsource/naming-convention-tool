/*-
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;

/**
 * Bean to automatically run Flyway data migration upon application startup.
 *
 * @author Anders Harrisson
 */
@Singleton
@Startup
@TransactionManagement(value = TransactionManagementType.BEAN)
public class FlywayMigration {

    private static final Logger LOGGER = Logger.getLogger(FlywayMigration.class.getName());

    @Resource(lookup = "java:/org.openepics.names.data")
    private DataSource dataSource;

    @PostConstruct
    private void onStartup() {

        if (dataSource == null) {
            LOGGER.log(Level.SEVERE, "DataSource could not be found during data migration!");
            throw new EJBException("DataSource could not be found during data migration!");
        }

        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);

        MigrationInfo migrationInfo = flyway.info().current();

        if (migrationInfo == null) {
            LOGGER.log(Level.INFO, "Database is not versioned");
        } else {
            LOGGER.log(Level.INFO, "Database contains data version: " + migrationInfo.getVersion() + " : "
                    + migrationInfo.getDescription());
        }

        flyway.migrate();
        LOGGER.log(Level.INFO, "Database migrated to data version: " + flyway.info().current().getVersion());
    }

}
