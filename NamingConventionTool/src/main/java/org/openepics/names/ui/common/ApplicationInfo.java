/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openepics.names.ui.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * A bean containing information about this application listed in applicationInfo.properties
 * @author karinrathsman
 */
@ManagedBean
@ApplicationScoped
public class ApplicationInfo {

    private Properties prop;
 
    @PostConstruct
    public void init(){
        InputStream resourceAsStream;
        resourceAsStream = this.getClass().getResourceAsStream(
                "/applicationInfo.properties"
        );
        this.prop = new Properties();
        try {
            this.prop.load(resourceAsStream);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    /**
     * 
     * @param key
     * @return return the property listed in applicationInfo.properties
     */
    public String get(String key){
        return prop.getProperty(key);
    }    
}
