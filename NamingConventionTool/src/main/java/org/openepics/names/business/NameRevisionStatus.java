package org.openepics.names.business;

import org.openepics.names.model.NamePartRevisionStatus;

/**
 * Enum that handles the status of a revision.
 *
 * @author karinrathsman
 *
 */
public enum NameRevisionStatus {
    APPROVED(),
    CANCELLED(),
    PENDING(),
    REJECTED();

    static NameRevisionStatus get(NamePartRevisionStatus status) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return true if the revision is unapproved and pending
     */
    public boolean isPending() {
        return equals(PENDING);
    }

    /**
     *
     * @return true if the revision approved
     */
    public boolean isApproved() {
        return equals(APPROVED);
    }

    /**
     *
     * @return true if the revision is unapproved and cancelled or rejcted
     */
    public boolean isCancelled() {
        return equals(CANCELLED) || equals(REJECTED);
    }

    /**
     *
     * @return true if the revision is unapproved and rejcted
     */
    public boolean isRejected() {
        return equals(REJECTED);
    }
}
