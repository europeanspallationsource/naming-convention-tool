package org.openepics.names.business;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.openepics.names.nameViews.NameView;
import org.openepics.names.nameViews.NameViewProvider;

import com.google.common.collect.Lists;

/**
 * Bean to handle filtering and node status of tree nodes (expanded or collapsed) throughout a session.
 *
 * @author Karin Rathsman
 */
@SessionScoped
public class NameFilter implements Serializable {

    private static final long serialVersionUID = -2153127283622095245L;
    private boolean includeActive;
    private boolean includePending;
    private boolean includeArchived;
    private boolean includeOnSite;
    private boolean includeOffSite;
    private Options[] selectedOptions;
    private boolean includeCancelled;
    private final Map<UUID, NodeStatus> nodeMap;
    final private NodeStatus rootNodeStatus = new NodeStatus(true, true, false);

    @Inject
    private NameViewProvider nameViewProvider;

    @Inject
    public NameFilter() {
        Options[] defaultOptions = new Options[]{Options.ACTIVE, Options.ONSITE, Options.PENDING};
        nodeMap = new HashMap<>();
        selectedOptions = defaultOptions;
    }

    public enum Options {
        ACTIVE, ARCHIVED, PENDING, ONSITE, OFFSITE, CANCELLED
    }

    /**
     * @return the options
     */
    public Options[] getOptions() {
        return Options.values();
    }

    /**
     * Sets the selected options
     *
     * @param selectedOptions the selected options to set.
     */
    public void setSelectedOptions(Options[] selectedOptions) {
        this.selectedOptions = selectedOptions;
        update();
    }

    /**
     *
     * @return the selected options
     */
    public Options[] getSelectedOptions() {
        return selectedOptions;
    }

    public void update() {
        List<Options> filter = Lists.newArrayList(getSelectedOptions());
        includeActive = filter.contains(Options.ACTIVE);
        includeArchived = filter.contains(Options.ARCHIVED);
        includePending = filter.contains(Options.PENDING);
        includeOffSite = filter.contains(Options.OFFSITE);
        includeOnSite = filter.contains(Options.ONSITE);
        includeCancelled = filter.contains(Options.CANCELLED);
        if (!nodeMap.isEmpty()) {
            for (UUID key : nodeMap.keySet()) {
                NameView nameView = nameViewProvider.nameView(key);
                NodeStatus nodeStatus = nodeMap.get(key);
                nodeStatus.setAccepted(accept(nameView));
            }
        }
    }

    /**
     *
     * @return true if the pending name stages are included
     */
    public boolean isIncludePending() {
        return includePending;
    }

    /**
     *
     * @return true if cancelled name stages are included.
     */
    public boolean isIncludeCancelled() {
        return includeCancelled;
    }

    private boolean isActive(NameStage nameStage) {
        return includeActive && nameStage.isActive();
    }

    public boolean isArchived(NameStage nameStage) {
        return includeArchived && nameStage.isArchived();
    }

    private boolean isPending(NameStage nameStage) {
        return includePending && nameStage.isPending();
    }

    private boolean isCancelled(NameStage nameStage) {
        return includeCancelled && nameStage.isCancelled();
    }

    /**
     * Decides whether a nameView is accepted by the view options.
     *
     * @param nameView
     * @return true if the name view is accepted by the view options
     */
    private boolean accept(NameView nameView) {
        return acceptStatus(nameView.getRevisionPair().getNameStage()) && include(nameView);
    }

    private boolean acceptStatus(NameStage nameStage) {
        return isActive(nameStage) || isArchived(nameStage) || isPending(nameStage) || isCancelled(nameStage);
    }

    private boolean include(NameView nameView) {
        if (nameView.getNameType().isDeviceStructure()) {
            return true;
        } else {
            if (nameView.isRoot()) {
                return false;
            } else if (nameView.isInStructure(NameType.AREA_STRUCTURE) && nameView.getParent(NameType.AREA_STRUCTURE).isRoot()) {
                boolean superSectionIsOffsite = nameView.getRevisionPair().getBaseRevision().getNameElement().getMnemonic() != null;
                return includeOffSite && superSectionIsOffsite || includeOnSite && !superSectionIsOffsite;
            } else {
                return include(nameView.getParent(NameType.AREA_STRUCTURE));
            }
        }
    }

    /**
     *
     * @param nameView the nameView
     * @return the nodeStatus of the nameView.
     */
    public NodeStatus getNodeStatus(NameView nameView) {
        if (nameView == null || nameView.isRoot()) {
            return rootNodeStatus;
        } else {
            UUID key = nameView.getRevisionPair().getBaseRevision().getNameArtifact().getUuid();
            if (!nodeMap.containsKey(key)) {
                nodeMap.put(key, new NodeStatus(false, true, accept(nameView)));
            }
            return nodeMap.get(key);
        }
    }

    /**
     * sets the customized filter.
     *
     * @param nameView the name View
     * @param filtered is true if the nameView is customized filtered.
     */
    public void setCustomizedFiltered(NameView nameView, boolean filtered) {
        if (nameView.isInDeviceRegistry()) {
            boolean subsectionFiltered = getNodeStatus(nameView.getParent(NameType.AREA_STRUCTURE)).isCustomizedFiltered();
            boolean deviceTypeFiltered = getNodeStatus(nameView.getParent(NameType.DEVICE_STRUCTURE)).isCustomizedFiltered();
            getNodeStatus(nameView).setCustomizedFiltered(subsectionFiltered && deviceTypeFiltered);
        } else if (nameView.isInStructure()) {
            getNodeStatus(nameView).setCustomizedFiltered(filtered);
            for (NameView child : nameView.getChildren()) {
                setCustomizedFiltered(child, filtered);
            }
        } else {
            return;
        }
    }

    /**
     * subclass to save the node status for individual nameViews
     *
     * @author karinrathsman
     *
     */
    public class NodeStatus {

        private boolean expanded;
        private boolean customizedFiltered;
        private boolean accepted;

        protected NodeStatus(boolean expanded, boolean filtered, boolean accepted) {
            this.expanded = expanded;
            this.customizedFiltered = filtered;
            this.accepted = accepted;
        }

        /**
         * @param filtered the filtered to set
         */
        public void setCustomizedFiltered(boolean filtered) {
            this.customizedFiltered = filtered;
        }

        /**
         *
         * @return true if filtered
         */
        public boolean isCustomizedFiltered() {
            return customizedFiltered;
        }

        /**
         * @return true if expanded
         */
        public boolean isExpanded() {
            return expanded;
        }

        /**
         * @param expanded the expanded to set
         */
        public void setExpanded(boolean expanded) {
            this.expanded = expanded;
        }

        /**
         * @return the accepted
         */
        public boolean isAccepted() {
            return accepted;
        }

        /**
         * @param accepted the accepted to set
         */
        protected void setAccepted(boolean accepted) {
            this.accepted = accepted;
        }

        /**
         *
         * @return true if the nodeStatus is accepted and filtered, false otherwise
         */
        public boolean isAcceptedAndFiltered() {
            return isAccepted() && isCustomizedFiltered();
        }

    }

}
