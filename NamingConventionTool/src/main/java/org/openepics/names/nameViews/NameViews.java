/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openepics.names.nameViews;

import com.google.common.base.Preconditions;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.openepics.names.business.NameArtifact;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameType;
import org.openepics.names.util.As;

/**
 *
 * @author karinrathsman
 */
public class NameViews {

    private final NameView areaRoot;
    private final NameView deviceRoot;
    private final Map<UUID, NameView> nameViewMap;

    public NameViews() {
        areaRoot = new NameView(NameType.AREA_STRUCTURE);
        deviceRoot = new NameView(NameType.DEVICE_STRUCTURE);
        nameViewMap = new HashMap<>();
    }

    /**
     *
     * @return the root area view
     */
    public NameView getAreaRoot() {
        return areaRoot;
    }

    /**
     *
     * @return the root device view
     */
    public NameView getDeviceRoot() {
        return deviceRoot;
    }

    private NameView newOrExistingNameView(NameArtifact nameArtifact) {
        UUID uuid = As.notNull(nameArtifact.getUuid());
        if (!contains(nameArtifact)) {
            final NameView nameView = new NameView(nameArtifact.getNameType());
            nameViewMap.put(uuid, nameView);
        }
        return As.notNull(nameViewMap.get(uuid));
    }

    /**
     * updates nameViews with a a new revision.
     *
     * @param revision the new revision
     * @return the updated nameView
     */
    public NameView update(NameRevision revision) {
        NameView nameView = newOrExistingNameView(revision.getNameArtifact());
        if (nameView.getRevisionPair().update(revision)) {
            updateParent(nameView, areaRoot);
            updateParent(nameView, deviceRoot);
        }
        return nameView;
    }

    /**
     * updates the parent child relation between names
     *
     * @param nameView
     * @param root
     */
    private void updateParent(NameView nameView, NameView root) {
        NameRevision revision = nameView.getRevisionPair().getBaseRevision();
        NameType rootType = root.getNameType();
        boolean parentRequired = nameView.getNameType().equals(rootType);
        NameArtifact parentArtifact = revision.getParent(rootType);
        NameView parentView = parentArtifact != null ? newOrExistingNameView(parentArtifact) : parentRequired ? root : null;
        nameView.setParent(parentView, rootType);
    }

    /**
     * Creates a clone of the specified nameView that belongs to this instance. If it already exist it returns the existing one without updating.
     *
     * @param nameView the nameView to be cloned.
     * @return the created clone or the existing clone. null if the nameView is null.
     */
    public NameView addCloneOf(NameView nameView) {
        if (nameView == null) {
            return null;
        } else if (nameView.isRoot()) {
            return getRoot(nameView.getNameType());
        } else {
            UUID uuid = nameView.getRevisionPair().getBaseRevision().getNameArtifact().getUuid();
            if (!nameViewMap.containsKey(uuid)) {
                NameView clone = new NameView(nameView);
                nameViewMap.put(uuid, clone);

                NameView areaParent = nameView.getParent(NameType.AREA_STRUCTURE);
                if (areaParent != null) {
                    clone.setParent(addCloneOf(areaParent), NameType.AREA_STRUCTURE);
                }
                NameView deviceParent = nameView.getParent(NameType.DEVICE_STRUCTURE);
                if (deviceParent != null) {
                    clone.setParent(addCloneOf(deviceParent), NameType.DEVICE_STRUCTURE);
                }
                return clone;
            } else {
                return nameViewMap.get(uuid);
            }
        }
    }

    /**
     *
     * @param nameType the name Type
     * @return the root of the area or device structure specified by the NameType.
     */
    protected NameView getRoot(NameType nameType) {
        Preconditions.checkState(nameType != null && !nameType.isDeviceRegistry());
        return nameType.isDeviceStructure() ? deviceRoot : areaRoot;
    }

    /**
     *
     * @param uuid the unique identifier for the name instance.
     * @return the nameView for the specified uuid
     */
    public NameView get(UUID uuid) {
        return nameViewMap.get(uuid);
    }

    /**
     *
     * @param artifact the artifact
     * @return true if nameViews contains the specified artifact
     */
    public boolean contains(NameArtifact artifact) {
        return nameViewMap.containsKey(artifact.getUuid());
    }
}
