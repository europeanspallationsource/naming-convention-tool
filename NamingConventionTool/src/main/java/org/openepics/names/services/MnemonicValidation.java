/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openepics.names.services;

/**
 *
 * Type of the mnemonic validation error specifying whether it too long or uses non-acceptable characters.
 *
 * @author banafshehhajinasab
 */
public enum MnemonicValidation {

   TOO_LONG, NON_ACCEPTABLE_CHARS, VALID, EMPTY;

}
